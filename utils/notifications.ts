import { showNotification } from "@mantine/notifications";

export const showErrorNotification = (message: string) =>
  showNotification({
    title: "An error occurred",
    message,
    color: "red",
    loading: false,
    autoClose: 5000,
  });

export const showInfoNotification = (message: string) =>
  showNotification({
    title: "Notification",
    message,
    color: "blue",
    loading: false,
    autoClose: 5000,
  });
