This is a [Next.js](https://nextjs.org/) sample project for usage with `Log Next.js API routes lambdas using Logtail on Vercel` tutorial.

## Getting Started

First, install required packages

```bash
pnpm install
```

Secondly, start local server

```bash
pnpm run dev
```

Open [localhost:3000](http://localhost:3000) with your browser to see the app & test it.

## Going further

Follow instructions from the article to deploy your app to Vercel & integrate with Logtail.
