module.exports = {
  ...require("@gorrion/prettier-config"),
  arrowParens: "avoid",
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: false,
  semi: false,
  tabWidth: 2,
  printWidth: 80,
  trailingComma: "all",
}
