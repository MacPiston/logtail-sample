import Head from "next/head";

const DefaultHead = () => (
  <Head>
    <title>Log Next.js lambdas with Logtail on Vercel</title>
  </Head>
);

export default DefaultHead;
