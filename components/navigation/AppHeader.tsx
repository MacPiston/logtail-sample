import { Title, Header, Center } from "@mantine/core";

const AppHeader = () => (
  <Header height={60} p="xs">
    <Center>
      <Title order={2}>Log Next.js lambdas with Logtail on Vercel</Title>
    </Center>
  </Header>
);

export default AppHeader;
