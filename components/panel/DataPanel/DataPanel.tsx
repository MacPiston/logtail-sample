import { Anchor, Card, Image, Text } from "@mantine/core";
import Link from "next/link";
import React from "react";

import type { GravatarData } from "@/data/api/types";

interface Props {
  userDetails: GravatarData | null;
}

const DataPanel = ({ userDetails }: Props) => (
  <div
    style={{
      minWidth: "360px",
      maxWidth: "480px",
      margin: "auto",
    }}>
    <Card
      shadow="sm"
      p="lg"
      sx={{
        minHeight: "240px",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}>
      {!!userDetails ? (
        <>
          <Card.Section>
            <Image
              src={`${userDetails.thumbnailUrl}?s=256`}
              alt="Profile image"
              height={160}
            />
          </Card.Section>
          <Text weight={500} sx={{ marginTop: "8px" }}>
            {userDetails.displayName}
          </Text>
          <Text size="sm" sx={theme => ({ color: theme.colors.gray[7] })}>
            ID: {userDetails.id}
          </Text>
          <Text size="sm" sx={theme => ({ color: theme.colors.gray[7] })}>
            Preferred username: {userDetails.preferredUsername}
          </Text>
          <Text size="sm" sx={theme => ({ color: theme.colors.gray[7] })}>
            Profile URL:{" "}
            <Link passHref href={userDetails.profileUrl}>
              <Anchor size="sm">{userDetails.profileUrl}</Anchor>
            </Link>
          </Text>
        </>
      ) : (
        <Text size="sm" sx={theme => ({ color: theme.colors.gray[7] })}>
          No user data. Enter email and fetch new Gravatar data.
        </Text>
      )}
    </Card>
  </div>
);

export default DataPanel;
