import { Button, Card, Group, TextInput } from "@mantine/core";
import { useForm } from "@mantine/form";
import React from "react";

interface Props {
  onEmailSubmit: (email: string) => void;
}

interface EmailForm {
  email: string;
}

const EmailPanel = ({ onEmailSubmit }: Props) => {
  const form = useForm<EmailForm>({
    initialValues: {
      email: "",
    },
    validate: {
      email: value => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
    },
  });

  return (
    <div style={{ minWidth: "360px", maxWidth: "480px", margin: "auto" }}>
      <Card shadow="sm" p="lg">
        <form onSubmit={form.onSubmit(({ email }) => onEmailSubmit(email))}>
          <TextInput
            required
            placeholder="Your email"
            label="Email address"
            {...form.getInputProps("email")}
          />
          <Group mt="md" position="center">
            <Button type="submit">Submit</Button>
          </Group>
        </form>
      </Card>
    </div>
  );
};

export default EmailPanel;
