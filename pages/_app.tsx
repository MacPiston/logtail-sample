import { MantineProvider } from "@mantine/core";

import DefaultHead from "@/components/common/DefaultHead";

import { NotificationsProvider } from "@mantine/notifications";

import type { AppProps } from "next/app";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <MantineProvider
      withGlobalStyles
      withNormalizeCSS
      styles={{
        Container: () => ({
          root: {
            padding: "16px",
            border: "0.5px solid #228be6",
            borderRadius: "8px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          },
        }),
      }}>
      <NotificationsProvider>
        <DefaultHead />
        <Component {...pageProps} />
      </NotificationsProvider>
    </MantineProvider>
  );
}

export default MyApp;
