import { AppShell, SimpleGrid } from "@mantine/core";

import AppHeader from "@/components/navigation/AppHeader";

import EmailPanel from "@/components/panel/EmailPanel";
import DataPanel from "@/components/panel/DataPanel";

import useFetchGravatarData from "@/hooks/useFetchGravatarData";

import type { NextPage } from "next";

const Home: NextPage = () => {
  const { userDetails, fetchUserDetails } = useFetchGravatarData();

  return (
    <AppShell
      padding="md"
      header={<AppHeader />}
      sx={theme => ({
        backgroundColor: theme.colors.gray[0],
        height: "100vh",
      })}>
      <SimpleGrid cols={2} sx={{ maxWidth: "920px", margin: "0 auto" }}>
        <EmailPanel onEmailSubmit={fetchUserDetails} />
        <DataPanel userDetails={userDetails} />
      </SimpleGrid>
    </AppShell>
  );
};

export default Home;
