import { md5 } from "@/utils/md5";

import type {
  GravatarDataResponse,
  MessageResponse,
  UserDetailsResponse,
} from "@/data/api/types";

import type { NextApiRequest, NextApiResponse } from "next";

const handler = async (
  req: NextApiRequest,
  res: NextApiResponse<MessageResponse | UserDetailsResponse>,
) => {
  try {
    if (req.method === "GET") {
      // Check if email address was provided
      const email = req.query.email as string;
      if (!email) return res.status(400).json({ message: "Bad request" });

      // Sample error for educational purposes
      if (email === "throw@exception.test")
        throw new Error("You've asked for this!");

      // Fetch user data from Gravatar
      const gravatarDataResponse = await fetch(
        `https://www.gravatar.com/${md5(email.trim().toLowerCase())}.json`,
      );
      if (!gravatarDataResponse.ok) {
        console.log(
          gravatarDataResponse.status,
          gravatarDataResponse.statusText,
        );

        if (gravatarDataResponse.status === 404)
          return res.status(404).json({ message: "User not found" });
        else return res.status(400).json({ message: "Bad request" });
      }

      // Parse received Gravatar data
      const gravatarData =
        (await gravatarDataResponse.json()) as GravatarDataResponse;

      // Return selected result
      return res.status(200).json({ data: gravatarData.entry[0] });
    } else return res.status(405).json({ message: "Method not allowed" });
  } catch (error: unknown) {
    console.error(error);

    return res.status(500).json({ message: "Internal server error" });
  }
};

export default handler;
