import { useCallback, useState } from "react";

import {
  showErrorNotification,
  showInfoNotification,
} from "@/utils/notifications";

import type { GravatarData, UserDetailsResponse } from "@/data/api/types";

const useFetchGravatarData = () => {
  const [userDetails, setUserDetails] = useState<GravatarData | null>(null);

  const fetchUserDetails = useCallback(async (email: string) => {
    try {
      const detailsResponse = await fetch(
        `/api/data/${encodeURIComponent(email)}`,
      );
      showInfoNotification("Request completed");

      if (!detailsResponse.ok) {
        if (detailsResponse.status === 404)
          showErrorNotification("User not found");
        else showErrorNotification("An error occurred");
        setUserDetails(null);
      } else {
        const responseData =
          (await detailsResponse.json()) as UserDetailsResponse;
        setUserDetails(responseData.data);
      }
    } catch (error: unknown) {
      // CONSOLE LOG
      // eslint-disable-next-line no-console
      console.log(error);
      showErrorNotification("An error occurred");
      setUserDetails(null);
    }
  }, []);

  return { userDetails, fetchUserDetails };
};

export default useFetchGravatarData;
