export interface MessageResponse {
  message: string;
}

export interface UserDetailsResponse {
  data: GravatarData;
}

export interface GravatarDataResponse {
  entry: GravatarData[];
}

export interface GravatarData {
  id: string;
  hash: string;
  profileUrl: string;
  preferredUsername: string;
  thumbnailUrl: string;
  displayName: string;
}
